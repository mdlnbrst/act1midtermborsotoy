package com.example.androidstudio.animatingimage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    public void animateImage(View view) {
        ImageView img1 = (ImageView) findViewById(R.id.img1);
        ImageView img2 = (ImageView) findViewById(R.id.img2);

        if (img1.getAlpha()==1) {
            img1.animate()
                    .alpha(0f)
                    .setDuration(3600)
                    .rotationBy(2000);
            img2.animate()
                    .alpha(1f)
                    .setDuration(-3600)
                    .rotationBy(2000);
        }

        if  (img2.getAlpha()==2) {
            img2.animate()
                    .alpha(0f)
                    .setDuration(3600)
                    .rotationBy(2000);
            img1.animate()
                    .alpha(1f)
                    .setDuration(-3600)
                    .rotationBy(2000);
        }



    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
